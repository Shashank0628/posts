import { AbstractControl } from "@angular/forms";
import { Observable, Observer, of } from "rxjs";

export const mimType = (control: AbstractControl )
: Promise<{[key:string]: any}> | Observable<{[key:string]: any}> =>{
  if(typeof(control.value) == 'string'){
    return of(null);
  }
  const file = control.value as File;
  const filereader = new FileReader()
  const fileObservable = Observable.create((observer: Observer<{[key:string]: any}>)=>{
    filereader.addEventListener('loadend', ()=>{
      const arr = new Uint8Array(filereader.result as ArrayBuffer).subarray(0,4)
      var isvalid = false;
      var hex = ""
      for(let i=0; i<arr.length ; i++){
        hex += arr[i].toString(16)
      }
      switch(hex){
        case "89504e47":
          isvalid = true;
          break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
        case "ffd8ffe3":
        case "ffd8ffe8":
          isvalid = true;
          break;
        default:
          isvalid = false; // Or you can use the blob.type as fallback
          break;
      }
      if(isvalid){
        observer.next(null)
      }
      else{
        observer.next({isinvalid: true})
      }
      observer.complete()
    })
    filereader.readAsArrayBuffer(file)
  })

  return fileObservable;
}
