import { Component, OnInit } from '@angular/core';
import { Post } from '../post.model';
import {NgForm , FormGroup , FormControl , Validators} from "@angular/forms";
import { PostService } from '../post.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { mimType } from './mim-type.validator';
@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})

export class PostCreateComponent implements OnInit{
  private mode:string = 'created';
  private postID:string;
  isloading = false;
  imagePreview: string;
  form:FormGroup;
  public post:Post = {id: "" , title: "" , content: "" , imagePath: ""};
  constructor(public postService: PostService , public route : ActivatedRoute){}

  ngOnInit(){
    this.form = new FormGroup({
      title: new FormControl(null , {validators: [Validators.required, Validators.min(3)]}),
      content: new FormControl(null , {validators: [Validators.required]}),
      image: new FormControl(null , {validators: [Validators.required] , asyncValidators: [mimType]})
    })
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('id')){
        this.mode = 'edited'
        this.postID = paramMap.get('id')
        this.isloading = true
        this.postService.fetchPost(this.postID).subscribe((answer)=>{
          this.isloading = false
          this.post = answer
          this.form.setValue({title: this.post.title , content: this.post.content, image: this.post.imagePath})
        })
      }
      else{
        this.mode = 'created';
        this.postID = null;
      }
    });

  }

  savePost(){
    if(this.form.invalid){
      return;
    }
    if(this.mode == 'created'){
      this.postService.addPost(this.form.value.title, this.form.value.content , this.form.value.image)
    }
    else{
      this.postService.editPost(this.postID , this.form.value.title, this.form.value.content, this.form.value.image)
    }
    this.form.reset()
  }

  onImageUpload(event: Event){
    const file = (event.target as HTMLInputElement).files[0]
    console.log(file)
    this.form.patchValue({image: file})
    this.form.get('image').updateValueAndValidity()
    const fileReader = new FileReader()
    fileReader.onload = ()=>{
      this.imagePreview = fileReader.result as string
    }
    fileReader.readAsDataURL(file)
  }

}
