import { Component,OnInit , OnDestroy} from '@angular/core';
import { Post } from '../post.model';
import { PostService } from '../post.service';
import { Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})

export class PostListComponent implements OnInit , OnDestroy{
posts: Post[] = []
isloading = false
totalPosts;
currentPage = 1
pageSize = 2
pageSizeOptions = [2,4,5,10]
private postSub: Subscription;

constructor(public postService: PostService){

 }
ngOnInit(){
    this.isloading = true
    this.postService.getPost(this.pageSize , this.currentPage)
    this.postSub = this.postService.getSubjectListner().subscribe((postsDataRecieved)=>{
      this.isloading = false
      this.posts = postsDataRecieved.posts;
      this.totalPosts = postsDataRecieved.count
  })
}

ngOnDestroy(){
  this.postSub.unsubscribe();
}

onChangedPage(page: PageEvent){
this.currentPage = page.pageIndex + 1
this.pageSize = page.pageSize
this.postService.getPost(this.pageSize , this.currentPage)
}

onDelete(postId: string){
  this.postService.deletePost(postId).subscribe(result=>{
    this.postService.getPost(this.pageSize , this.currentPage)
  })
}
}
