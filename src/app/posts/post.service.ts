import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {Post} from './post.model';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';


@Injectable({providedIn: 'root'})
export class PostService{
  private posts : Post[] = []
  private postSubject = new Subject<{posts: Post[] , count: number}>();

  constructor(private http: HttpClient , public router: Router){

  }

  getSubjectListner(){
    return this.postSubject.asObservable();
  }

  getPost(pagesize:number, currentpage:number){
      const queryParams = `?pagesize=${pagesize}&currentpage=${currentpage}`
      this.http.get<{message: string , posts: any, maxPosts: number}>("http://localhost:3000/api/posts" + queryParams).
      pipe(map((postsArrived)=>{
        return { posts: postsArrived.posts.map((post)=>{
          return {
            title: post.title,
            content: post.content,
            id: post._id,
            imagePath: post.imagePath
          }
        }) , maxPosts: postsArrived.maxPosts
      }
      })).
      subscribe((transformedPostArrivedData)=>{
        this.posts = transformedPostArrivedData.posts;
        this.postSubject.next({
          posts: [...this.posts],
          count: transformedPostArrivedData.maxPosts
        })
      })

  }


  fetchPost(id: string){
    return this.http.get<{id:string , title: string , content: string , imagePath: string}>("http://localhost:3000/api/posts/" + id)
  }

  addPost(title: string  , content: string , image: File){
    const postData = new FormData()
    postData.append("title", title)
    postData.append("content" , content)
    postData.append("image" , image)

    this.http.post<{message: string , postId: string , imagePath: string}>("http://localhost:3000/api/posts" , postData).
    subscribe((messageArrived)=>{
      this.router.navigate(["/"])
    })

  }

  editPost(id:string , title:string, content: string , image: string | File){
    let postData: FormData | Post
    if(typeof(image) == 'object'){
      postData = new FormData()
      postData.append("id" , id)
      postData.append("title" , title)
      postData.append("content" , title)
      postData.append("image" , image)
    }
    else{
      postData = {
        id: id,
        title: title,
        content: content,
        imagePath: image
      }
    }
    this.http.put<{message:string , imagePath:string}>("http://localhost:3000/api/posts/" + id , postData).
    subscribe(result=>{
      this.router.navigate(["/"])
    })
  }

  deletePost(postId: string){
    return this.http.delete<{message:string}>("http://localhost:3000/api/posts/" + postId);
  }
}
